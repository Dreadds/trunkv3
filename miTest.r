library(gtools)
P1 <- permutations(6, 6)
P1
head(P1)
head(P1, 10)
tail(P1)

#elegir las 3 mejores marcas a partir de un total de 12
n <- 12
r <- 3
Marcas <- c(letters[1:n])
P2 <- permutations(n,r,Marcas)
P2
head(P2)
nrow(P2)

#numero de combinaciones de 5 elementos en grupos de 3
#combinacion simple
C1 <- combinations(5,3)
c1
nrow(C1)

#5 tipos diferentes de botellas, de cuantas formas
#puede elegir 4 de ellas?
#combinacion con repeticion
m <- 5
n <- 4
botellas <- letters[1:m]
C2 <- combinations(m,n,botellas,repeats.allowed = TRUE)
C2
nrow(C2)
